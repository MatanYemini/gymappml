import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import sys
import math

value_to_predict = int(sys.argv[1])
X = [[6], [8], [10], [14], [18]]
y = [[7], [9], [13], [17.5], [18]]
plt.figure()
plt.title('Pizza price plotted against diameter')
plt.xlabel('Diameter in inches')
plt.ylabel('Price in dollars')
plt.plot(X, y, 'k.')
plt.axis([0, 25, 0, 25])
plt.grid(True)
#plt.show()
# Training data
X = [[5], [8], [10], [12], [14]]
y = [[60], [96], [110], [120], [125]]
# Now Create and fit the model
model = LinearRegression()
model.fit(X, y)
res = model.predict([[value_to_predict]])[0]
#print("A 12 inch pizza should cost : %5.3f." + model.predict([[12]])[0])
#print("A 18 kg kettlebell should cost : %5.3f" % res[0])
print(float(res[0]))
